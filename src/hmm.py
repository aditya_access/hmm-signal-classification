import states
import probabilities
import config
import grapher
import compute_hmm
import classify_in_state
import synthesize_signal
import signal_input
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(color_codes=True)
plt.style.use('seaborn-bright')


def main():
    global end_training
    training_seq = []
    states_p = []
    states_u = []
    states_n = []

    while not signal_input.end_training:
        training_seq, wave_type = signal_input.accept_wave()
        if wave_type == 'p':
            states_p.append(states.state_divider(training_seq))
            # print(*states_p, sep='\n')
            for i in states_p:
                for j in i:
                    print(j)
            states.state_cluster(states_p, wave_type)
            # print('>>')
            # print(config.state_1_all)
            # print(config.state_pd_p_all)
            # print(config.state_pu_p_all)
        if wave_type == 'u':
            states_u.append(states.state_divider(training_seq))
            # print(*state_u, sep='\n')
            for i in states_u:
                for j in i:
                    print(j)
            states.state_cluster(states_u, wave_type)
        if wave_type == 'n':
            states_n.append(states.state_divider(training_seq))
            # print(*states_n, sep='\n')
            for i in states_n:
                for j in i:
                    print(j)
            states.state_cluster(states_n, wave_type)

    print('state 1 all')
    print(config.state_1_all)
    print('state n1')
    print(config.state_n1_n_all)
    print('state n2')
    print(config.state_n2_n_all)
    all_states = []
    all_states.append(config.state_1_all)
    all_states.append(config.state_pd_p_all)
    all_states.append(config.state_pu_p_all)
    all_states.append(config.state_ud_u_all)
    all_states.append(config.state_uu_u_all)
    all_states.append(config.state_n1_n_all)
    all_states.append(config.state_n2_n_all)
    # sns.distplot(all_states[1], rug=True)
    # plt.show()
    # sns.distplot(all_states[2], rug=True)
    # plt.show()
    # sns.distplot(all_states[5], rug=True)
    # plt.show()
    # sns.distplot(all_states[6], rug=True)
    # plt.show()
    # grapher.get_graph(all_states, True, 'Distributions of all states', 'EOG Values', 'Frequency', True, False)
    grapher.get_graph(config.state_1_all, False,
                      'All of state 1', 't', 'EOG values', False, False)
    grapher.get_graph(config.state_pd_p_all, False,
                      'All of state pd', 't', 'EOG values', False, False)
    grapher.get_graph(config.state_pu_p_all, False,
                      'All of state pu', 't', 'EOG values', False, False)
    grapher.get_graph(config.state_n1_n_all, False,
                      'All of state n1', 't', 'EOG values', False, False)
    grapher.get_graph(config.state_n2_n_all, False,
                      'All of state n2', 't', 'EOG values', False, False)
    # grapher.get_graph(config.state_pu_p_all, False, 'All of state pu', 't', 'EOG values', False, False)
    #
    # grapher.get_graph(config.state_1_all, False, 'state 1 histogram', 'EOG values', 'Frequency', True, False)
    # grapher.get_graph(config.state_pd_p_all, False, 'state pd histogram', 'EOG values', 'Frequency', True, False)
    # grapher.get_graph(config.state_pu_p_all, False, 'state pu histogram', 'EOG values', 'Frequency', True, False)

    config.state_1_elem_probs = probabilities.get_sample_probabilities(
        config.state_1_all)
    config.state_1_1_st = 1 / len(config.state_1_elem_probs)
    config.state_pd_elem_probs = probabilities.get_sample_probabilities(
        config.state_pd_p_all)
    config.state_pu_elem_probs = probabilities.get_sample_probabilities(
        config.state_pu_p_all)
    config.state_ud_elem_probs = probabilities.get_sample_probabilities(
        config.state_ud_u_all)
    config.state_uu_elem_probs = probabilities.get_sample_probabilities(
        config.state_uu_u_all)
    config.state_n1_elem_probs = probabilities.get_sample_probabilities(
        config.state_n1_n_all)
    config.state_n2_elem_probs = probabilities.get_sample_probabilities(
        config.state_n2_n_all)

    print()
    print('state 1 elements and their probs')
    for i, j in config.state_1_elem_probs.items():
        print(i, '\t', j)
    print('state pd elements and their probs')
    for i, j in config.state_pd_elem_probs.items():
        print(i, '\t', j)
    print('state pu elements and their probs')
    for i, j in config.state_pu_elem_probs.items():
        print(i, '\t', j)
    print('state ud elements and their probs')
    for i, j in config.state_ud_elem_probs.items():
        print(i, '\t', j)
    print('state uu elements and their probs')
    for i, j in config.state_uu_elem_probs.items():
        print(i, '\t', j)
    print('state n1 elements and their probs')
    for i, j in config.state_n1_elem_probs.items():
        print(i, '\t', j)
    print('state n2 elements and their probs')
    for i, j in config.state_n2_elem_probs.items():
        print(i, '\t', j)

    config.state_pd_1_st, config.state_pu_1_st = probabilities.state_transition_elements(
        states_p)
    config.state_ud_1_st, config.state_uu_1_st = probabilities.state_transition_elements(
        states_u)
    config.state_n1_1_st, config.state_n2_1_st = probabilities.state_transition_elements(
        states_n)
    print()
    print('state 1 transition elements')
    for i, j in config.state_1_st.items():
        print(i, '\t', j)
    print('state pd -> 1 transition elements')
    for i, j in config.state_pd_1_st.items():
        print(i, '\t', j)
    print('state pu -> 1 transition elements')
    for i, j in config.state_pu_1_st.items():
        print(i, '\t', j)
    print('state ud -> 1 transition elements')
    for i, j in config.state_ud_1_st.items():
        print(i, '\t', j)
    print('state uu -> 1 transition elements')
    for i, j in config.state_uu_1_st.items():
        print(i, '\t', j)
    print('state n1 -> 1 transition elements')
    for i, j in config.state_n1_1_st.items():
        print(i, '\t', j)
    print('state n2 -> 1 transition elements')
    for i, j in config.state_n2_1_st.items():
        print(i, '\t', j)

    config.state_pd_pd_st = probabilities.get_future_elements(
        config.state_pd_p_all, config.state_pd_elem_probs)
    config.state_pu_pu_st = probabilities.get_future_elements(
        config.state_pu_p_all, config.state_pu_elem_probs)
    config.state_ud_ud_st = probabilities.get_future_elements(
        config.state_ud_u_all, config.state_ud_elem_probs)
    config.state_uu_uu_st = probabilities.get_future_elements(
        config.state_uu_u_all, config.state_uu_elem_probs)
    config.state_n1_n1_st = probabilities.get_future_elements(
        config.state_n1_n_all, config.state_n1_elem_probs)
    config.state_n2_n2_st = probabilities.get_future_elements(
        config.state_n2_n_all, config.state_n2_elem_probs)
    print()
    print('**state 1 -> 1 transition probability')
    print(config.state_1_1_st)
    print('state pd -> pd transition elements')
    for i, j in config.state_pd_pd_st.items():
        print(i, '\t', j)
    print('state pu -> pu transition elements')
    for i, j in config.state_pu_pu_st.items():
        print(i, '\t', j)
    print('state ud -> ud transition probability')
    print(config.state_ud_ud_st)
    print('state uu -> uu transition elements')
    for i, j in config.state_uu_uu_st.items():
        print(i, '\t', j)
    print('state n1 -> n1 transition elements')
    for i, j in config.state_n1_n1_st.items():
        print(i, '\t', j)
    print('state n2 -> n2 transition elements')
    for i, j in config.state_n2_n2_st.items():
        print(i, '\t', j)

    states.gather_all_states()

    print()
    print('gathered states:')
    print('element probs:')
    for i, j in config.all_state_probabilities.items():
        print(i, '\t', j)
    print('element state transition probs:')
    for i, j in config.inter_state_transition_probs.items():
        print(i, '\t', j)
    print('element intra state transition probs:')
    for i, j in config.intra_state_transition_probs.items():
        print(i, '\t', j)
    print()

    classify_in_state.get_training_sets()
    # vt = [0.0, 0.02, 0.0, -0.06, -0.08, -0.1, -0.18, -0.18, -0.12, -0.1, -0.08, -0.06, 0.04, 0.02, -0.02, 0.0, 0.06, 0.08, 0.1, 0.18, 0.18, 0.12, 0.1, 0.08, 0.06]
    while True:
        vt = signal_input.testing_wave()
        if vt[0] == 101:
            break
        grapher.get_graph(vt, False, 'Sample wave', 't',
                          'EOG values', False, False)
        compute_hmm.forward_alg(vt)
    # for i in range(10):
    #     doubt_wave = signal_input.doubt_wave()
    #     print('sample wave')
    #     print(*doubt_wave, sep='\n')
    #     grapher.get_graph(doubt_wave, False, 'Sample FOD', 't', 'FOD EOG Values', False, False)
    # compute_hmm.forward(vt)
    # print()
    # print('probability dictionary')
    # for i, j in config.probability.items():
    #     print(i, '\t', j)

    # c = 100
    # while c != 66.0:
    #     c = float(input('\nenter test data\n'))
    #     classify_in_state.get_labels_values(c)

    # print()
    # print('training data set')
    # for i, j in zip(classify_in_state.x_train, classify_in_state.y_train):
    #     print(j, '\t', i)
    print()
    print('change in state probs over time')
    for i, j in config.change_in_probs_over_time.items():
        print(i)
        for k in j:
            print(k)
    print('instantaneous state probs over time')
    for i, j in config.instantaneous_probs.items():
        print(i)
        for k in j:
            print(k)
            plt.title(i)
            plt.plot(k)
            plt.show()
    print()
    print('Observed order of progression')
    print(*synthesize_signal.order_of_progression)

    # print()
    # print('change in state probs over time in list form')
    # print(*change_list, sep='\n')
    # x, y = zip(*change_list)
    # # plt.plot(x, y[0])
    # for a, b in zip(x, y):
    #     plt.plot(b[0])
    # plt.show()


main()

