import cleanup
from scipy.signal import savgol_filter
import numpy as np
import grapher

end_training = False

cleanup.initialize(25, 0.2, 0.02)


def difference(dataset, interval):
    diff = []
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
        # if (value <= 0.04 or value >= -0.04):
        #     diff.append(0.0)
        # else:
        #     diff.append(value)
    return diff


def inverse_difference(last_ob, value):
	return value + last_ob


def accept_wave():
    global end_training
    print('enter training sequence')
    wave = []
    _type = '0'
    while True:
        a = input()
        if a in ['p', 'u', 'n']:
            _type = a
            continue
        if a == '-10':
            break
        if a == '-20':
            end_training = True
            break
        b = float(a)
        if b < -0.9 or b > 0.9:
            continue
        else:
            wave.append(round(cleanup.clean_and_get_FOD(b), 2))
        # else:
        #     wave.append(cleanup.get_clean_element(b))
    # wave = np.asarray(wave)
    # smooth = savgol_filter(wave, 51, 3)
    # diff_wave = difference(smooth, 25)
    # inverted = [inverse_difference(smooth[i], diff_wave[i]) for i in range(len(diff_wave))]
    # # wave = np.asarray(wave)
    # # smooth = savgol_filter(wave, 51, 3)
    # # print('smoothed signal')
    # # print(*smooth, sep='\n')
    # # grapher.get_graph(smooth, False, 'Test wave', 'T', 'EOG values', False, False)
    # grapher.get_graph(diff_wave, False, 'Test wave', 'T', 'EOG values', False, False)
    # grapher.get_graph(inverted, False, 'Test wave', 'T', 'EOG values', False, False)
    # smooth_2 = []
    # for i in diff_wave:
    #     smooth_2.append(round(i, 2))
    grapher.get_graph(wave, False, 'Training wave', 'T', 'EOG values', False, False)
    return wave, _type


def testing_wave():
    wave = []
    print('enter testing sequence')
    while True:
        a = input()
        if a == '-99':
            break
        b = float(a)
        if (b < -0.9 or b > 0.9):
            if b == 100 or b == 101:
                wave.append(b)
            else:
                continue
        else:
            wave.append(round(cleanup.clean_and_get_FOD(b), 2))
    return wave


def doubt_wave():
    wave = []
    print('enter sample for test viewing')
    while True:
        a = input()
        if a == '-99':
            break
        b = float(a)
        # if b < -0.9 or b > 0.9:
        #     continue
        # else:
        wave.append(round(cleanup.clean_and_get_FOD(b), 2))
    return wave
