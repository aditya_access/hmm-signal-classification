

buf = list()
clean_buf = list()
buf_size = 25
regularization_threshold = 0.18
FOD_threshold = 0.04


def update_normal_buf(element):
    global buf
    buf.append(element)
    if len(buf) > buf_size:
        buf = buf[1:]


def update_clean_buf(element):
    global clean_buf
    clean_buf.append(get_clean_element(element))
    if len(clean_buf) > buf_size:
        clean_buf = clean_buf[1:]


def get_mean(element):
    global buf
    mean = 0
    for each_element in buf:
        mean += each_element
    if len(buf) is 0:
        mean = 0
    else:
        mean = mean / len(buf)
    update_normal_buf(element)
    return mean


def get_clean_element(element):
    global buf, regularization_threshold
    mean = get_mean(element)
    if abs(element - mean) > regularization_threshold:
        return element
    else:
        return mean


def clean_and_get_FOD(element):
    global buf
    update_clean_buf(element)
    if len(clean_buf) is not 0 and abs(clean_buf[len(clean_buf) - 1] - clean_buf[0]) > FOD_threshold:
        return clean_buf[len(buf) - 1] - clean_buf[0]
    else:
        return 0


def initialize(buffer_size, rthresh, fthresh):
    global buf_size, regularization_threshold, FOD_threshold
    buf_size = buffer_size
    regularization_threshold = rthresh
    FOD_threshold = fthresh
