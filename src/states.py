import config

def state_divider(wave):
    s1 = []
    s2 = []
    s3 = []
    a = []
    b = []
    c = []
    x = False
    _max = wave[0]
    _min = wave[0]
    state = []
    for i in range(len(wave)):
        if wave[i] > 0:
            x = False
            a.append(wave[i])
            _max = max(_max, wave[i])
        if (wave[i] < 0):
            x = False
            b.append(wave[i])
            _min = min(_min,wave[i])
        if x:
            if b:
                s2.append(b)
                b = []
                _min = wave[i]
            if a:
                s3.append(a)
                a = []
                _max = wave[i]
            if c:
                s1.append(c)
                c = []
        if wave[i] == 0:
            c.append(wave[i])
            x = True
    state.append(s1)
    state.append(s2)
    state.append(s3)
    return state

def state_cluster(state, _type):
    if _type == 'p':
        for idx, i in enumerate(state):
            for indx, j in enumerate(i):
                if indx == 0:
                    for k in j:
                        for l in k:
                            config.state_1_all.append(l)
                if indx == 1:
                    for k in j:
                        for l in k:
                            config.state_pd_p_all.append(l)
                if indx == 2:
                    for k in j:
                        for l in k:
                            config.state_pu_p_all.append(l)
    if _type == 'u':
        for idx, i in enumerate(state):
            for indx, j in enumerate(i):
                if indx == 0:
                    for k in j:
                        for l in k:
                            config.state_1_all.append(l)
                if indx == 1:
                    for k in j:
                        for l in k:
                            config.state_ud_u_all.append(l)
                if indx == 2:
                    for k in j:
                        for l in k:
                            config.state_uu_u_all.append(l)
    if _type == 'n':
        for idx, i in enumerate(state):
            for indx, j in enumerate(i):
                if indx == 0:
                    for k in j:
                        for l in k:
                            config.state_1_all.append(l)
                if indx == 1:
                    for k in j:
                        for l in k:
                            config.state_n1_n_all.append(l)
                if indx == 2:
                    for k in j:
                        for l in k:
                            config.state_n2_n_all.append(l)


def gather_all_states():
    config.all_state_probabilities['state 1'] = config.state_1_elem_probs
    config.all_state_probabilities['prolonged down'] = config.state_pd_elem_probs
    config.all_state_probabilities['prolonged up'] = config.state_pu_elem_probs
    config.all_state_probabilities['up down'] = config.state_ud_elem_probs
    config.all_state_probabilities['up up'] = config.state_uu_elem_probs
    config.all_state_probabilities['disturbance 1'] = config.state_n1_elem_probs
    config.all_state_probabilities['disturbance 2'] = config.state_n2_elem_probs

    config.all_state_storage['state 1'] = config.state_1_all
    config.all_state_storage['prolonged down'] = config.state_pd_p_all
    config.all_state_storage['prolonged up'] = config.state_pu_p_all
    config.all_state_storage['up down'] = config.state_ud_u_all
    config.all_state_storage['up up'] = config.state_uu_u_all
    config.all_state_storage['disturbance 1'] = config.state_n1_n_all
    config.all_state_storage['disturbance 2'] = config.state_n2_n_all

    config.inter_state_transition_probs['state 1'] = config.state_1_st
    config.inter_state_transition_probs['prolonged down'] = config.state_pd_1_st
    config.inter_state_transition_probs['prolonged up'] = config.state_pu_1_st
    config.inter_state_transition_probs['up down'] = config.state_ud_1_st
    config.inter_state_transition_probs['up up'] = config.state_uu_1_st
    config.inter_state_transition_probs['disturbance 1'] = config.state_n1_1_st
    config.inter_state_transition_probs['disturbance 2'] = config.state_n2_1_st

    config.intra_state_transition_probs['state 1'] = config.state_1_1_st
    config.intra_state_transition_probs['prolonged down'] = config.state_pd_pd_st
    config.intra_state_transition_probs['prolonged up'] = config.state_pu_pu_st
    config.intra_state_transition_probs['up down'] = config.state_ud_ud_st
    config.intra_state_transition_probs['up up'] = config.state_uu_uu_st
    config.intra_state_transition_probs['disturbance 1'] = config.state_n1_n1_st
    config.intra_state_transition_probs['disturbance 2'] = config.state_n2_n2_st


    