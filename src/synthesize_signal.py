import config
import grapher
import random
from collections import Counter

probs_over_time_by_states = []
state_list = [*config.probability.keys()]
state_count = {'state 1': 0.0, 'prolonged down': 0.0, 'prolonged up': 0.0,
               'up down': 0.0, 'up up': 0.0, 'disturbance 1': 0.0, 'disturbance 2': 0.0,
               'unknown state': 0.0}
state_mean = {'state 1': 0.0, 'prolonged down': 0.0, 'prolonged up': 0.0,
               'up down': 0.0, 'up up': 0.0, 'disturbance 1': 0.0, 'disturbance 2': 0.0,
               'unknown state': 0.0}
state_std = {'state 1': 0.0, 'prolonged down': 0.0, 'prolonged up': 0.0,
               'up down': 0.0, 'up up': 0.0, 'disturbance 1': 0.0, 'disturbance 2': 0.0,
               'unknown state': 0.0}
artificial_state_element_count = {'state 1': {}, 'prolonged down': {}, 'prolonged up': {},
               'up down': {}, 'up up': {}, 'disturbance 1': {}, 'disturbance 2': {},
               'unknown state': {}}
order_of_progression = []
artificial_signal_states = []
artificial_signal_values = []


def probabilistic_progression():
    global probs_over_time_by_states, state_list
    for i, j in config.probability.items():
        config.change_in_probs_over_time[i][config.n_calls_to_forward].append(round(j, 10))
    # grapher.get_graph(probs_over_time, True, 'State Probabilities over time', '', '', False, False)


def get_parameters(path_states):
    global state_count, state_mean, state_std, order_of_progression
    tmp = dict(Counter(path_states))
    _iter = 0
    for i, j in state_count.items():
        if i in [*tmp]:
            state_mean[i] = ((state_mean[i] * state_count[i]) + tmp[i]) / (state_count[i] + 1)
            state_std[i] = (((tmp[i] - state_mean[i]) ** 2) / (state_count[i] + 1)) ** 0.5
            state_count[i] += 1
    for i in path_states:
        order_of_progression.append([])
        if i in order_of_progression[_iter]:
            continue
        else:
            order_of_progression[_iter].append(i)
        _iter += 1


def get_value_count():
    global state_mean, state_std, artificial_state_element_count
    elems = []
    count = []
    for i, j in state_mean.items():
        for k, l in config.all_state_probabilities[i].items():
            a = j + (random.choice([1, -1]) * state_std[i])
            elems.append(m)
            count.append(a * l)
        artificial_state_element_count[i] = dict(zip(elems, count))


def generate():
    global state_mean, state_std, artificial_signal_states, artificial_signal_values
    ini_dist = dict(Counter(config.initial_dist))
    artificial_signal_states.append(max(ini_dist, key=ini_dist.get))
    artificial_signal_values.append(max(config.all_state_probabilities[max(ini_dist, key=ini_dist.get)], key=config.all_state_probabilities[max(ini_dist, key=ini_dist.get)].get))
    print(*artificial_signal_states, '\t', *artificial_signal_values, sep='\n')



