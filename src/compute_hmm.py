import config
import classify_in_state
import synthesize_signal
import math
import operator
from mpmath import *
import random
import probabilities
import sys
from collections import Counter
mp.dps = 20

theta = {'state 1': 1.0, 'prolonged down': 1.0, 'prolonged up': 1.0,
               'up down': 1.0, 'up up': 1.0, 'disturbance 1': 1.0, 'disturbance 2': 1.0,
               'unknown state': 1.0}


def n_prob_change_recorder():
    for i, j in config.change_in_probs_over_time.items():
        config.change_in_probs_over_time[i].append([])
        config.instantaneous_probs[i].append([])


def get_multiplied(current_value, prob_of_existence_of_new_value, prob_of_transition_to_new_value):
    return current_value + prob_of_existence_of_new_value + prob_of_transition_to_new_value
    # return mpf(current_value) * mpf(prob_of_existence_of_new_value) * mpf(prob_of_transition_to_new_value)


def get_closest(sample, to_search_in):
    dist_mat = []
    print('\nlist of similar elements to', sample)
    print(*to_search_in, sep='\n')
    for i in to_search_in:
        dist_mat.append(abs(sample - i))
    # dist_mat.sort()
    print('sorted list of distances')
    print(*dist_mat, sep='\n')
    if dist_mat:
        index_min = min(range(len(dist_mat)), key=dist_mat.__getitem__)
        x = to_search_in[index_min]
        return x
    else:
        return 'nil'


def path_formation():
    global dist_idx, change
    state_list = [*config.probability.keys()]
    prob_list = [*config.probability.values()]
    probable_states = []
    probable_elems = []
    for idx, i in enumerate(prob_list):
        if i != 0.0:
            probable_elems.append(i)
            probable_states.append(state_list[idx])
    # return probable_states
    if probable_elems:
        curr = 0
        closer_to_zero = probable_elems[0]
        # probable_elems.sort()
        for i in probable_elems:
            curr = i * i
            if curr <= (closer_to_zero * closer_to_zero):
                closer_to_zero = i
        return probable_states[probable_elems.index(closer_to_zero)]
    else:
        return 'unknown state'
    # return max(config.probability.items(), key=operator.itemgetter(1))[0]


def classify(state_path):
    tmp = dict(Counter(state_path))
    for i, j in theta.items():
        if i == 'state 1':
            continue
        else:
            theta[i] = round(mpf((mpf(tmp.get(i, 0.0))) / mpf((tmp.get('prolonged down', 0.0) + tmp.get('prolonged up', 0.0) + tmp.get('up down', 0.0) + tmp.get('up up', 0.0) + tmp.get('disturbance 1', 0.0) + tmp.get('disturbance 2', 0.0) + tmp.get('unknown state', 0.0)))), 5)
            # config.probability[i] = 1 / (round(mpf(mp.exp(config.probability[i]) * theta[i]) / (1 / 6), 5))
            config.probability[i] = theta[i]
    wave_type = {'Prolonged': 0.0, 'Up': 0.0, 'Disturbance': 0.0, 'unknown': 0.0}
    wave_type['Prolonged'] = config.probability['prolonged down'] + config.probability['prolonged up']
    wave_type['Up'] = config.probability['up down'] + config.probability['up up']
    wave_type['Disturbance'] = config.probability['disturbance 1'] + config.probability['disturbance 2']
    wave_type['unknown'] = config.probability['unknown state']
    return wave_type


def forward_alg(vt):
    if vt[0] == 100:
        synthesize_signal.generate()
    else:
        path = []
        indiscriminate_path = []
        prob_path = []
        instantaneous_path = []
        aa = []
        future_state = []
        _iter = 0
        for i, j in config.probability.items():
            config.probability[i] = 1.0
        n_prob_change_recorder()
        for i in range(len(vt)):
            if i == 0:
                states, elements, probs = classify_in_state.get_labels_values(vt[i])
                for idx, i in enumerate(states):
                    if i == 'state 1':
                        config.probability[i] = mp.ln(math.exp(1))
                    else:
                        config.probability[i] = mp.ln(probs[idx])
                for idx, a in enumerate(states):
                    for i, j in config.probability.items():
                        if i != a and j == 1:
                            config.probability[a] = mp.ln(1)
                    config.initial_dist.append(a)
                prob_path.append([*config.probability.values()])
                path.append(path_formation())
                print()
                print('initial dist')
                for i, j, in config.probability.items():
                    print(i, '\t', j)
            else:
                t_states, t_elements, t_probs = classify_in_state.get_labels_values(vt[i])
                t_minus_one_states, t_minus_one_elems, t_minus_one_probs = classify_in_state.get_labels_values(
                    vt[i - 1])
                # if not t_states:
                #     t_states.append('unknown state')
                #     t_elements.append(1.0)
                #     t_probs.append(1.0)
                # if not t_minus_one_states:
                #     t_minus_one_states.append('unknown state')
                #     t_minus_one_elems.append(1.0)
                #     t_minus_one_probs.append(1.0)
                for x in range(len(t_states)):
                    tmp = 0
                    tmp_inst = 0
                    for y in range(len(t_minus_one_states)):
                        if t_states[x] == t_minus_one_states[y]:
                            if t_minus_one_states[y] == 'state 1':
                                intra_st = 0.5
                                tmp += get_multiplied(((config.probability[t_minus_one_states[y]])),
                                                      mp.ln((math.exp(1))), mp.ln((intra_st)))
                                tmp_inst += mp.ln(math.exp(1)) + mp.ln(intra_st)
                                break
                            elif t_minus_one_states[y] == 'unknown state':
                                intra_st = 0.5
                            elif t_elements[x] in config.intra_state_transition_probs[t_minus_one_states[y]][
                                t_minus_one_elems[y]]:
                                intra_st = \
                                config.intra_state_transition_probs[t_minus_one_states[y]][t_minus_one_elems[y]][
                                    t_elements[x]]
                            else:
                                st_keys = [
                                    *config.intra_state_transition_probs[t_minus_one_states[y]][t_minus_one_elems[y]]]
                                intra_st_tmp = get_closest(t_elements[x], st_keys)
                                if intra_st_tmp == 'nil':
                                    if t_minus_one_elems[y] in config.all_state_storage[t_minus_one_states[y]] and \
                                            config.all_state_storage[t_minus_one_states[y]].index(
                                                    t_minus_one_elems[y]) == len(
                                            config.all_state_storage[t_minus_one_states[y]]) - 1:
                                        config.all_state_storage[t_minus_one_states[y]].append(t_elements[x])
                                    elif t_minus_one_elems[y] in config.all_state_storage[t_minus_one_states[y]] and \
                                            config.all_state_storage[t_minus_one_states[y]].index(
                                                    t_minus_one_elems[y]) != len(
                                            config.all_state_storage[t_minus_one_states[y]]) - 1:
                                        config.all_state_storage[t_minus_one_states[y]].insert(
                                            config.all_state_storage[t_minus_one_states[y]].index(
                                                t_minus_one_elems[y] + 1), t_elements[x])
                                    else:
                                        config.all_state_storage[t_minus_one_states[y]].append(t_minus_one_elems[y])
                                        config.all_state_storage[t_minus_one_states[y]].append(t_elements[x])
                                    tmp_1 = probabilities.get_sample_probabilities(
                                        config.all_state_storage[t_minus_one_states[y]])
                                    config.all_state_probabilities[t_minus_one_states[y]] = tmp_1
                                    tmp_2 = probabilities.get_future_elements(
                                        config.all_state_storage[t_minus_one_states[y]],
                                        config.all_state_probabilities[t_minus_one_states[y]])
                                    config.intra_state_transition_probs[t_minus_one_states[y]] = tmp_2
                                    intra_st = \
                                    config.intra_state_transition_probs[t_minus_one_states[y]][t_minus_one_elems[y]][
                                        t_elements[x]]
                                else:
                                    intra_st = \
                                    config.intra_state_transition_probs[t_minus_one_states[y]][t_minus_one_elems[y]][
                                        intra_st_tmp]
                                # intra_st = config.intra_state_transition_probs[t_minus_one_states[y]][t_minus_one_elems[y]][t_elements[x]]
                            if intra_st == 1.0:
                                intra_st = math.exp(1)
                            tmp += get_multiplied(((config.probability[t_minus_one_states[y]])), mp.ln((t_probs[x])),
                                                  mp.ln((intra_st)))
                            tmp_inst += mpf(t_probs[x]) * mpf(intra_st)
                        if t_states[x] != t_minus_one_states[y]:
                            if t_minus_one_states[y] == 'state 1':
                                inter_st = 0.5
                                tmp += get_multiplied(((config.probability[t_minus_one_states[y]])),
                                                      mp.ln((math.exp(1))), mp.ln((inter_st)))
                                tmp_inst += mpf(math.exp(1)) * mpf(inter_st)
                                break
                            if t_minus_one_states[y] == 'unknown state':
                                inter_st = 0.5
                            else:
                                inter_st = config.inter_state_transition_probs[t_minus_one_states[y]].get(
                                    t_minus_one_elems[y], 1.0)
                            tmp += get_multiplied(((config.probability[t_minus_one_states[y]])), mp.ln((t_probs[x])),
                                                  mp.ln((inter_st)))
                            tmp_inst += mpf(t_probs[x]) * mpf(inter_st)
                    config.probability[t_states[x]] = tmp
                    config.instantaneous_probs[t_states[x]][config.n_calls_to_forward].append(round(tmp_inst, 7))
                    q = t_states[x]
                    aa.append([q, tmp_inst])
                    indiscriminate_path.append(t_states[x])
                    if x == len(t_states) - 1:
                        for f, g in config.probability.items():
                            if not f in t_states:
                                config.probability[f] = mp.ln(1)
                        print()
                        print('current probabilities of states')
                        for i in t_elements: print('element:', i)
                        for i, j, in config.probability.items():
                            print(i, '\t', j)
                        instantaneous_path = aa
                        # for i, j in config.instantaneous_probs.items():
                        #     if i in t_states:
                        #         continue
                        #     else:
                        #         config.instantaneous_probs[i][config.n_calls_to_forward].append(0)
                        path.append(path_formation())
                        synthesize_signal.probabilistic_progression()
                        aa = []
                        prob_path.append([*config.probability.values()])
                        print('Instantaneous probabilities')
                        print(instantaneous_path)
                _iter += 1

        print()
        print('final state')
        for i, j, in config.probability.items():
            print(i, '\t', j)
        # print('Path')
        # print(path)
        print('Path of probabilities')
        print(*prob_path, sep='\n')
        print()
        print('##Most probable sequence of states followed')
        print(*path, sep='>>\n')
        synthesize_signal.get_parameters(path)
        wave_type_all = classify(indiscriminate_path)
        wave_type_max = classify(path)
        print()
        print('>>Probabilities of all states of the observed signal<<')
        print('State\tTolerant probs\tMax probs')
        for i, j in wave_type_all.items():
            print(i, '\t', j, '\t', wave_type_max[i])
        print()
        t_p_m = max(wave_type_all, key=wave_type_all.get)
        m_p_m = max(wave_type_max, key=wave_type_max.get)
        if m_p_m == t_p_m:
            a = []
            b = []
            u = round(wave_type_all['Prolonged'], 3)
            for k, v in wave_type_all.items():
                if round(v, 3) == u:
                    a.append(k)
                    b.append(v)
            if len(a) > 1:
                for idx, i in enumerate(a):
                    print(b[idx] * 100, '% Markovian chance of being in class', i, 'and', wave_type_max[i] * 100, '% Cumulative chance of the same class')
                print()
            else:
                print('Observation most probably falls in the', m_p_m, 'class')
                print()
        else:
            print('Observation follows', m_p_m, 'with', (wave_type_all[t_p_m] * 100), '% Markovian', t_p_m, 'bias and', (wave_type_max[m_p_m] * 100), '% Cumulative', m_p_m, 'bias')
            print()
        config.n_calls_to_forward += 1
                