import math
import numpy as np
import config
import probabilities
from sklearn.neighbors import KNeighborsClassifier

knn = KNeighborsClassifier(n_neighbors=2)

x_train = []
y_train = []
x_knn = []
prob_train = []
labels = []
values = []
probs = []

def sort_for_max():
    global values, labels, probs
    tmp = '0'
    v_tmp = []
    l_tmp = []
    p_tmp = []
    for idx, i in enumerate(labels):
        if tmp == i:
            if probs[idx] > p_tmp[l_tmp.index(i)]:
                p_tmp[l_tmp.index(i)] = probs[idx]
                v_tmp[l_tmp.index(i)] = values[idx]
        else:
            tmp = i
            l_tmp.append(i)
            v_tmp.append(values[idx])
            p_tmp.append(probs[idx])
    return l_tmp, v_tmp, p_tmp


# def add_to_state(state, value, prob):

def fit_data():
    global y_train, x_knn, knn
    knn.fit(x_knn, y_train)



def get_labels_values(sample):
    global x_train, y_train, prob_train, labels, values, probs
    labels = []
    values = []
    probs = []
    if sample in x_train:
        for idx, i in enumerate(x_train):
            if i == sample:
                labels.append(y_train[idx])
                values.append(i)
                probs.append(prob_train[idx])
        return labels, values, probs
    else:
        # for i in range(len(x_train)):
        #     # distance.append(abs(sample - x_train[i]))
        #     if math.isclose(round(abs(round(sample, 2) - round(x_train[i], 2)), 2), 0.0, rel_tol=0.01, abs_tol=0.01):
        #         values.append(x_train[i])
        #         labels.append(y_train[i])
        #         probs.append(prob_train[i])
        #         distance.append(abs(sample - x_train[i]))

        # print()
        # print('test data set')
        # for i, j, k in zip(values, labels, probs):
        #     print(j, '\t', i, '\t', k)
        # # for i in distance:
        # #     print(i)
        l_0 = knn.predict([[sample]])[0]
        # l_0, v_0, p_0 = sort_for_max()
        # if l_0:
        # for idx, i in enumerate(l_0):
        #     config.all_state_storage[i].append(v_0[idx])
        #     tmp = probabilities.get_sample_probabilities(config.all_state_storage[i])
        #     config.all_state_probabilities[i] = tmp
        config.all_state_storage[l_0].append(sample)
        tmp = probabilities.get_sample_probabilities(config.all_state_storage[l_0])
        config.all_state_probabilities[l_0] = tmp
        tmp_2 = probabilities.get_future_elements(config.all_state_storage[l_0], config.all_state_probabilities[l_0])
        config.intra_state_transition_probs[l_0] = tmp_2
        get_training_sets()
        l = [y_train[x_train.index(sample)]]
        v = [sample]
        p = [prob_train[x_train.index(sample)]]
        return l, v, p

        # print()
        # print('usable test data set')
        # for i, j, k in zip(v_0, l_0, p_0):
        #     print(j, '\t', i, '\t', k)
        # print()
        # else:
        #     return l_0, v_0, p_0

def get_training_sets():
    global x_train, y_train, prob_train, x_knn
    x_train = []
    y_train = []
    prob_train = []
    x_knn = []
    for i, j in config.all_state_probabilities.items():
        for k, l in j.items():
            x_train.append(k)
            x_knn.append([k])
            prob_train.append(l)
            y_train.append(i)
    print()
    print('training data set')
    for i, j, k in zip(x_train, y_train, prob_train):
        print(j, '\t', i, '\t', k)
    fit_data()




