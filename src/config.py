import math

state_1_all = []
state_pu_p_all = []
state_pd_p_all = []
state_ud_u_all = []
state_uu_u_all = []
state_n1_n_all = []
state_n2_n_all = []

state_1_elem_probs = {}
state_pu_elem_probs = {}
state_pd_elem_probs = {}
state_uu_elem_probs = {}
state_ud_elem_probs = {}
state_n1_elem_probs = {}
state_n2_elem_probs = {}

state_1_st = {}
state_pd_1_st = {}
state_pu_1_st = {}
state_ud_1_st = {}
state_uu_1_st = {}
state_n1_1_st = {}
state_n2_1_st = {}

state_1_1_st = 0
state_pd_pd_st = {}
state_pu_pu_st = {}
state_ud_ud_st = {}
state_uu_uu_st = {}
state_n1_n1_st = {}
state_n2_n2_st = {}


all_state_storage = {'state 1': math.inf, 'prolonged down': math.inf, 'prolonged up': math.inf,
               'up down': math.inf, 'up up': math.inf, 'disturbance 1': math.inf, 'disturbance 2': math.inf}
all_state_probabilities = {'state 1': math.inf, 'prolonged down': math.inf, 'prolonged up': math.inf,
               'up down': math.inf, 'up up': math.inf, 'disturbance 1': math.inf, 'disturbance 2': math.inf}
inter_state_transition_probs = {'state 1': None, 'prolonged down': None, 'prolonged up': None,
               'up down': None, 'up up': None, 'disturbance 1': None, 'disturbance 2': None}
intra_state_transition_probs = {'state 1': None, 'prolonged down': None, 'prolonged up': None,
               'up down': None, 'up up': None, 'disturbance 1': None, 'disturbance 2': None}
probability = {'state 1': 1.0, 'prolonged down': 1.0, 'prolonged up': 1.0,
               'up down': 1.0, 'up up': 1.0, 'disturbance 1': 1.0, 'disturbance 2': 1.0,
               'unknown state': 1.0}
instantaneous_probs = {'state 1': [], 'prolonged down': [], 'prolonged up': [],
               'up down': [], 'up up': [], 'disturbance 1': [], 'disturbance 2': [],
                             'unknown state': []}
change_in_probs_over_time = {'state 1': [], 'prolonged down': [], 'prolonged up': [],
               'up down': [], 'up up': [], 'disturbance 1': [], 'disturbance 2': [],
                             'unknown state': []}

n_calls_to_forward = 0
initial_dist = []


def printer():
    print('tester')
    print(state_1_all)
    
