from random import choices
from collections import Counter
import config

def get_sample_frequencies(sample):
    tmp = Counter(sample)
    return dict(tmp)

def get_sample_probabilities(sample):
    tmp = get_sample_frequencies(sample)
    _sum = 0
    for i in tmp:
        _sum += tmp[i]
    for i in tmp:
        tmp[i] = tmp[i] / _sum
    return tmp

def state_transition_elements(state):
    tmp_1 = []
    tmp_2 = []
    tmp_3 = []
    for x in state:
        for indx, y in enumerate(x):
            if indx == 0:
                for z in y:
                    if len(z) > 1:
                        tmp_1.append(z[len(z) - 1])
                    else:
                        tmp_1.append(z[0])
            if indx == 1:
                for z in y:
                    if len(z) > 1:
                        tmp_2.append(z[len(z) - 1])
                    else:
                        tmp_2.append(z[0])
            if indx == 2:
                for z in y:
                    if len(z) > 1:
                        tmp_3.append(z[len(z) - 1])
                    else:
                        tmp_3.append(z[0])
        # if len(x) > 1:
        #     tmp.append(x[len(x) - 1])
        # else:
        #     tmp.append(x[0])
    # print('st temp')
    # print(tmp)
    tmp_s1_dict = get_sample_probabilities(tmp_1)
    for i, j in tmp_s1_dict.items():
        if i in config.state_1_st:
            config.state_1_st[i] = j
        else:
            config.state_1_st[i] = j
    return  get_sample_probabilities(tmp_2), get_sample_probabilities(tmp_3)


def get_future_elements(state, known_elements):
    tmp_k = list(known_elements.keys())
    tmp_v = []
    _iter = 0
    for i, j in known_elements.items():
        tmp_v.append([])
        for j in range(len(state)):
            if state[j] == i:
                for k in range(j + 1, j + 1 + len(known_elements)):
                    if k >= len(state): break
                    tmp_v[_iter].append(state[k])
        tmp_v[_iter] = get_sample_probabilities(tmp_v[_iter])
        _iter += 1
    tmp_dict = dict(zip(tmp_k, tmp_v))
    return tmp_dict