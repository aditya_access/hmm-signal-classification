import matplotlib.pyplot as plt
from matplotlib import style
import seaborn as sns
sns.set(color_codes=True)
plt.style.use('seaborn-bright')


def get_graph(sample, is_nd, title, xlabel, ylabel, sns_flag, xy_flag):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if is_nd and not xy_flag:
        for i in sample:
            plt.title(title)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            for j in i:
                plt.plot(j)
            plt.show()
    if xy_flag and not is_nd:
        plt.plot(*zip(*sample))
        plt.show()
    if is_nd and xy_flag:
        for i in sample:
            plt.plot(*zip(*i))
        plt.show()
    if sns_flag and not is_nd:
        sns.distplot(sample, rug=True)
        plt.show()
    if is_nd and sns_flag:
        for i in sample:
            sns.distplot(i, rug=True)
        plt.show()
    if not is_nd and not sns_flag and not xy_flag:
        plt.plot(sample)
        plt.show()